/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task1;

import Common.Main;
import Common.Helpers;
import java.io.*;//imports all internal elements of io
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Helkern
 */
public class TestSubject extends Helpers { //inherits from helpers class

    private final String FILENAME = "t.txt"; //constant file name
    Scanner scan = new Scanner(System.in); //declaring variables and objects
    String input = new String().toUpperCase();
    List<Subject> subjectList;
    String gSubjectName;
    String gSubjectCode;

    public TestSubject() throws Exception { //getData() has throw exception
        this.subjectList = getData(); //assining arraylist to the list object
    }

    public void Init() throws Exception {
        Subject subject = new Subject(); //creating new Subject object
        customMessage("To add new record type 'add'\n"); //user interaction message stuffs
        input = validationMessage(input, "Please type 'add'\n", Arrays.asList("ADD"));
        int i = 0; //declaring and asigning to 0
        customMessage("Please select a discipline code. ");
        customMessage("List of discipline codes in the database:\n");
        for (String s : subject.allDisciplines(subjectList)) {//prints all unique list of discipline codes
            customMessage(s + " | ");
            i++;
            if (i % 10 == 0) { //each 10 value put newline
                customMessage("\n");
            }
        }
        customMessage("\nSelect:\n");
        input = scan.next();
        while(!input.toUpperCase().matches("\\D\\D\\D"))//discipline code validation
        {
            customMessage("Please enter valid Discipline Code (i.e. ITC)\n");
            input = scan.next();
        }
        customMessage("Selected Discipline code have following Subject Codes:\n");
        i = 0;
        for (String s : subject.codesPerDiscipline(subjectList, input)) {//showing interelated subject codes in this discipline code category
            customMessage(s + " | ");
            i++;
            if (i % 10 == 0) {
                customMessage("\n");
            }
        }
        customMessage("\nPlease type a new Subject Code (ie. ITC505):\n"); //user types final scode
        input = scan.next();//storing to input
        gSubjectCode = input;//reassining to other string var
        customMessage("Please type the name of the Subject\n");//user types final sname
        input = scan.next();
        input += scan.nextLine();//allows to prepend all text in a line, otherwise scan.next() does not read after space
        gSubjectName = input;
        setData(gSubjectName, gSubjectCode);//sends values to the internal method setData()
        customMessage("\nWould you like to add more subjects? ('yes' | 'no')\n");
        input = scan.next();
        validationMessage(input.toUpperCase(), "\nWould you like to add more subjecs? ('yes' | 'no')\n", Arrays.asList("YES", "NO"));
        if (input.toUpperCase().equals("YES")) {
            this.Init();//recursive class method calling
        }
        customMessage("Would you like to go to the Main menu? ('yes' - mainMenu | 'no' - quit)\n");
        input = scan.next();
        validationMessage(input.toUpperCase(), "Would you like to go to the Main menu? ('yes' - mainMenu | 'no' - quit)\n", Arrays.asList("YES", "NO"));
        if (input.toUpperCase().equals("YES")) {
            Main.main(null);//main menu method calling
        }
    }

    private List<Subject> getData() throws Exception {
        List<Subject> arrSubject = new ArrayList<>();//new list of subject arraylist
        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {//bufferreader tries to get the file, if fails exception will be caught

            String sCurrentLine; //for storing each line

            while ((sCurrentLine = br.readLine()) != null) { //bufferreader reads each line from the file. Actually bf in debugging mode, contains a list of chars from the file
                arrSubject.add(new Subject(sCurrentLine.substring(0, sCurrentLine.indexOf(',')), sCurrentLine.substring(sCurrentLine.indexOf(',') + 1))); //adds new subject into the list of subject, accordance with the pattern set: {subjectname},{subjectcode}
            }
            return arrSubject;

        } catch (IOException e) {
            customMessage("The file is missing or corrupted");//proper message displayed and the program halted in case of error catch
            return null;
        }
    }

    private void setData(String subjectName, String subjectCode) throws Exception {//writes a new data to the file
        Subject subject = new Subject(subjectName, subjectCode);//->validation part
        input = subjectCode;
        while (!subject.isValidCode(subjectCode) || subject.codeExists(subjectList, input)) {//ensures the subject code is valid accordance with pattern set, and the code entered does not exist in the file, making sure all subject code records are unique
            if (subject.codeExists(subjectList, input)) {//falls into this part if either isValidCode or codeExists is true, further there will be  checks and message displays
                customMessage("The code you have entered already in the database. Please type another code:\n");
                input = scan.next();
                subject = new Subject(subjectName, input.toUpperCase());
                subjectCode = input;
            } else if (!subject.isValidCode(subjectCode)) {
                customMessage("Please enter valid Subject Code (eg. ITC506)");
                input = scan.next();
                subject = new Subject(subjectName, input.toUpperCase());
                subjectCode = input;
            }
        }//validation part finish <-
        BufferedWriter bufferedWriter = null; //declaring objects and assinging initial values
        FileWriter fileWriter = null;

        try {
            String content = subjectName + "," + subjectCode.toUpperCase(); //constructing final text

            fileWriter = new FileWriter(FILENAME, true);//selecting the file to write into, true means the new record will append to the file, so old data will not be overwritten
            bufferedWriter = new BufferedWriter(fileWriter);//creating new bf object
            bufferedWriter.write(content + "\n");//calling write method, which takes parameters to send into the file
            customMessage("The subject was successfully written into the database!");//if all good this message will be displayed, otherwise exception will be executed before
        } catch (IOException e) {
            e.printStackTrace();
        } finally {//last step is trying to close connections to the file, on error exception is called
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        //end try-catch

    }

}
