/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Helkern
 */
public class Subject { //All methods and fields were created according with all requirements were set.

    private String name; //Declaring internal properties/fields
    private String subjectCode;

    public Subject() {//Empty constuctor
    }

    Subject(String name, String subjectCode) throws Exception {//with params, on of interrelated classes uses an exception, which makes to use throws Exception in this constructor as well.
        this.name = name; //assigning param vals to internal fields
        this.subjectCode = subjectCode;
    }

    /**
     * @return the name
     */
    public String getName() {//gets internal val of name
        return name;
    }

    /**
     * @return the subjectCode
     */
    public String getSubjectCode() {
        return subjectCode;
    }

    public String getDiscipline() { //Even its not used enywhere in the code, but the requirement asks for it
        return subjectCode.substring(0, subjectCode.length() / 2); // gets first half of the string
    }

    public boolean codeMatches() {
        if (subjectCode != null && subjectCode.matches("\\D\\D\\D\\d\\d\\d")) { //checks whether the property matches to regex conditions. null check prevents program brake in case of null entries
            return true;
        }
        return false;
    }

    public String toString() {
        return subjectCode + "|" + name; //not used method in the program, but requirement asks for it
    }

    public List<String> allDisciplines(List<Subject> allSubjectList) { //Takes list of subject and returns processed list of string
        List<String> disciplineCodes = new ArrayList<>(); //new list of strings
        for (Subject s : allSubjectList) { //each time iterates, while there is a record in the list of subject, each time assigns a record from the allsubjectlist array to the subject object s
            if (!disciplineCodes.contains(s.subjectCode.substring(0, 3))) {// condition to check, whether a record of the array is in the list 
                disciplineCodes.add(s.subjectCode.substring(0, 3)); //before adding it into the list
            }
        }
        Collections.sort(disciplineCodes);//sorts the list alphabetically
        return disciplineCodes;
    }

    public List<String> codesPerDiscipline(List<Subject> allSubjectList, String disciplineCode) throws Exception {//gets list and string param and checks whether the string value (dicipline code) exists in the list
        List<String> sortedSubjectCodes = new ArrayList<>(); //new arraylist
        for (Subject s : allSubjectList) {
            if (s.subjectCode.substring(0, 3).equals(disciplineCode.toUpperCase())) {//ig first half of the string is equal to the string discipline code
                sortedSubjectCodes.add(s.subjectCode.substring(3, s.subjectCode.length()));//if so add record to the new list
            }
        }
        if (sortedSubjectCodes.isEmpty()) { //just for better output design
            sortedSubjectCodes.add("0 - Subject Codes");
        }
        return sortedSubjectCodes;
    }

    public boolean isValidCode(String subjectCode) {//checks, whether input param is valid according to the regex requirements and whether its null
        if (subjectCode != null && subjectCode.matches("\\D\\D\\D\\d\\d\\d")) {
            return true;
        }
        return false;
    }

    public boolean codeExists(List<Subject> allSubjectList, String subjectCode) { //like before, but checks whole subjectcode
        for (Subject o : allSubjectList) {
            if (o != null && o.subjectCode.equals(subjectCode.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    public List<Subject> sortDisciplines(List<Subject> o) {//not used in the program, is written according to the requirements
        Collections.sort(o, (Subject s1, Subject s2) -> s1.subjectCode.compareToIgnoreCase(s2.subjectCode));//using lambda expression, to sort the collection using comparator. currently the comparator value is subject code - this is the property which will be sorted alphabetically.
        return o;
    }

}
