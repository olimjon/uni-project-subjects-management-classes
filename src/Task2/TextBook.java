/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task2;

import Common.Helpers;

/**
 *
 * @author Helkern
 */
public abstract class TextBook extends Helpers{ //Creating an abstract class TextBook, this class inherits common class Helpers, which has message methods, so all subclasses can use Helpers class

    private String title; //Declaring properties. These are available only to this class, and outside is not available
    private String author;
    private int pages;

    public TextBook() { //Declaring empty construction, so it will be possible to create empty instance of TextBook object

    }

    public TextBook(String title, String author, int pages) { //This constructor assigns value from parameters into internal fields, such as title, author and pages
        this.title = title;
        this.author = author;
        this.pages = pages;
    }

    
    public String toString() { // Created a method for this abstract class, return String value
        return getTitle() + " is written by " + getAuthor() + " contains " + getPages() + " pages.";
    }

    /**
     * @return the title
     */
    public String getTitle() { // Getter method, which gets internal title field and returns it as a String value
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) { //This method sets a value to the internal field. This and above method is called encapsulation
        this.title = title;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the pages
     */
    public int getPages() {
        return pages;
    }

    /**
     * @param pages the pages to set
     */
    public void setPages(int pages) {
        this.pages = pages;
    }

}
