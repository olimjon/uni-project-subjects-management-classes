/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task2;

import Common.Main;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Helkern
 */
public class TestTextBooks extends TextBook {// inherits from parent class TextBook

    Scanner scan = new Scanner(System.in); //Declaring scan Object. This is for reading user inputs
    static String input = new String(); //Have to use static, to get access for the main method. This String object will store user input data

    public static void main(String[] args) throws Exception { // Setting point of entry to this class
        ProgrammingTextbook programming1 = new ProgrammingTextbook("The C++ Programming language", "Bjarne Stroustrup", 500, "C++"); //Creating a new ProgrammingTextBook object and assigning new values as parameters.
        ProgrammingTextbook programming2 = new ProgrammingTextbook("Effective Java", "Joshua Bloch", 756, "Java");
        ProgrammingTextbook programming3 = new ProgrammingTextbook("Effective Java2", "Joshua Bloch", 981, "Java");
        EngineeringTextbook engineering1 = new EngineeringTextbook("PERRY'S CHEMICAL ENGINEERS", "Robert H. Perry", 379, "Chemistry");
        EngineeringTextbook engineering2 = new EngineeringTextbook("Engineering Mechanics: Statics", "R. C. Hibbeler", 811, "Mechanics");
        EngineeringTextbook engineering3 = new EngineeringTextbook("Engineering Mechanics2: Dynamics", "R. C. Hibbeler", 674, "Mechanics");
        ArrayList<TextBook> objects = new ArrayList<>(); //Creating a new ArrayList textbook object for storing references for TextBook fields
        objects.add(programming1); //Adding a record into the 'object' arraylist
        objects.add(programming2);
        objects.add(programming3);
        objects.add(engineering1);
        objects.add(engineering2);
        objects.add(engineering3);

        for (TextBook t : objects) { //For each loop, will iterate, while there is a record in the arraylist. Each time a single record from objects arraylist will be assigned to the TextBook object t
            customMessage(t.toString() + "\n"); //Printing value of t. t object has a toString method, which returns String value. \n - escaped construct, which allows to put a new line to the string result.
        }
        input = new String(); //Creating new input object, it will kinda refresh its value, so validationMessage method will not take an old value and program will function properly
        input = validationMessage(input, "\nWould you like to go back to the Main Menu? ('yes' - Main Menu | 'no' - quit)\n", Arrays.asList("YES", "NO")); //custom validation method came from Helpers Clas
        if (input.toUpperCase().equals("YES")) { //Comparing content of the input. As a standard, I restructured output value of input variable. This helps the program to function properly, no matter of case sensitivity
            Main.main(null); //If true, return to the beginning of the program, otherwise the program will finish execution.
        }
    }
}
