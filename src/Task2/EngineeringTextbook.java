/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task2;

/**
 *
 * @author Helkern
 */
public class EngineeringTextbook extends TextBook { //Created subclass (or concrete class), which inherits from TextBook abstract class its properties
    private String subject;//Declaring private String field. This field is specifically belongs to this subclass.

    public EngineeringTextbook() {
    }

    public EngineeringTextbook(String title, String author, int pages, String subject) { 
        super(title, author, pages);  //calls parent constructor
        this.subject = subject;
    }

    @Override
    public String toString() {
        return getSubject() + ": " + super.toString(); //Overriding parent method
    }    
    
    /**
     * @return the subject
     */
    public String getSubject() { //Getters and setters below, to set a value and get value (getting access for internal field subject
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }
    
}
