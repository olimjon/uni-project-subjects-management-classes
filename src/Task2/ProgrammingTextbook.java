/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task2;

/**
 *
 * @author Helkern
 */
public class ProgrammingTextbook extends TextBook { //Created subclass (or concrete class), which inherits from TextBook abstract class its properties
    private String language; //Declaring private String field. This field is specifically belongs to this subclass.

    public ProgrammingTextbook() {
    }
    
    public ProgrammingTextbook(String title, String author, int pages, String language){ //Constructor with parameters
        super(title, author, pages); //calls parent constructor
        this.language = language;
    }

    @Override
    public String toString() {
        return getLanguage() + ": " + super.toString(); //Overriding parent method
    }
    
    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }
}
