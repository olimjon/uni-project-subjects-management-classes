package Common;

import java.util.List;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Helkern
 */
public class Helpers {//all related methods will be used by all task packages, so therefore this class is common for all other classes

    static Scanner scan = new Scanner(System.in); //new scan object

    public static void customMessage(Object o) {//for easiness, decided to create custom message method, instead of writing sys.out.print each time. this gives fast access to print console messages
        System.out.print(o);
    }

    public static String validationMessage(String inputParam, String msgParam, List constraints) {
        while (!constraints.contains(inputParam.toUpperCase()) || inputParam == null) { // If valid input was not expected, go to loop.
            customMessage(msgParam); // Displays parameter message, which is assigned internally.
            inputParam = scan.next(); // Assigning new entry to the sc parameter.
        }
        return inputParam; // Returning result.
    }
}
