/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Common;

import Task1.TestSubject;
import Task2.TestTextBooks;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Helkern
 */
public class Main extends Helpers{ //inherits from common class Helpers
    //It should be noticed, the program took Vast Amount Of Validation checks and Lots Of bug Tests and took LOOTS of time for testing and refining
    //You may try to play around the validation of the program

    static Scanner scan = new Scanner(System.in);
    static String input = new String().toUpperCase();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        input = new String().toUpperCase();
        input = validationMessage(input, "Welcome to Assignment 3! \nWhich task would you like to browse? "
                + "\n(Select: '1' - Task 1|'2' - Task 2|'quit' - to quit)\n", Arrays.asList("1", "2", "QUIT"));
        if (input.equals("1")) {
            TestSubject t = new TestSubject();
            t.Init();            
        }
        else if (input.equals("2")){
            TestTextBooks.main(null);
        }
        else{
            customMessage("Thanks for Watching!");
        }

    }


}
